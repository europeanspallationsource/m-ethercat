PROJECT=ethercatlib
LIBOBJS += $(wildcard ../../O.${T_A}/lib/.libs/*.o)
include ${EPICS_ENV_PATH}/module.Makefile

SOURCES=-none-
HEADERS=$(addprefix ethercat-1.5.2/include/,ecrt.h ectty.h)
vpath % ../../O.${T_A}/tool/
EXECUTABLES=../../O.${T_A}/tool/ethercat
