#----------------------------------------------------------------------------
#
#  $Id$
#
#  Copyright (C) 2006-2010  Florian Pose, Ingenieurgemeinschaft IgH
#
#  This file is part of the IgH EtherCAT Master.
#
#  The IgH EtherCAT Master is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License version 2, as
#  published by the Free Software Foundation.
#
#  The IgH EtherCAT Master is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#  Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with the IgH EtherCAT Master; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#  ---
#
#  The license mentioned above concerns the source code only. Using the
#  EtherCAT technology and brand is only permitted in compliance with the
#  industrial property and similar rights of Beckhoff Automation GmbH.
#
#  vim: tw=78
#
#----------------------------------------------------------------------------
%define archive_dir ethercat-%{unit_version_full}
%define kversion $(uname -r)

%define debug_package %{nil}
%define _unpackaged_files_terminate_build 0 

Name:           %{codac_rpm_prefix}-ethercat
Version:        %{unit_version_full}
Release:        %{codac_packaging_release}
License:        GPL
URL:            http://etherlab.org/en/ethercat

Source:         ethercat-%{unit_version_full}.tar.gz
Patch0:         ethercat-%{unit_version_full}.patch

BuildRoot:      %{_tmppath}/%{name}-%{unit_version_full}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  kernel-devel
BuildRequires:  redhat-rpm-config

%provides_self

#----------------------------------------------------------------------------
# Main Package
#----------------------------------------------------------------------------

Summary: IgH EtherCAT Master
Group: EtherLab

%description
This is an open-source EtherCAT master implementation for Linux 2.6. See the
FEATURES file for a list of features. For more information, see
http://etherlab.org/en/ethercat.

%kernel_module_package

#----------------------------------------------------------------------------
# Development package
#----------------------------------------------------------------------------

%package devel

Summary:	Development files for applications that use the EtherCAT master.
Group:		EtherLab
Requires:       %{name}

%description devel
This is an open-source EtherCAT master implementation for Linux 2.6. See the
FEATURES file for a list of features. For more information, see
http://etherlab.org/en/ethercat.

#----------------------------------------------------------------------------
# Source package
#----------------------------------------------------------------------------

%package src

Summary:	Source files for the EtherCAT master.
Group:		EtherLab
Requires:       %{name}

%description src
This is an open-source EtherCAT master implementation for Linux 2.6. See the
FEATURES file for a list of features. For more information, see
http://etherlab.org/en/ethercat.

#----------------------------------------------------------------------------

%prep
%setup -q -n %{archive_dir}
%patch0 -p1

%build
%configure --enable-tty --enable-generic --disable-8139too
#    --enable-e100 --enable-e1000 --enable-e1000e --enable-r8169

make
mkdir obj
for flavor in %kversion; do
    target=obj/$flavor
    rm -rf $target
    mkdir $target
    cp -r config.h globals.h Kbuild master/ devices/ \
        examples/ tty/ include/ $target
    make -C /usr/src/kernels/$flavor modules M=$PWD/$target
done

%install
for flavor in %kversion; do
	md5sum obj/$flavor/Module.symvers
done
make DESTDIR=${RPM_BUILD_ROOT} install
for flavor in %kversion; do
    target=obj/$flavor
    make -C /usr/src/kernels/$flavor modules_install \
        M=$PWD/$target INSTALL_MOD_PATH=${RPM_BUILD_ROOT} \
        INSTALL_MOD_DIR=ethercat
done

# Prepare sources for src rpm
make clean
rm -rf obj
mkdir -p ${RPM_BUILD_ROOT}/usr/src
cp -r $RPM_BUILD_DIR/%{archive_dir} $RPM_BUILD_ROOT/usr/src/%{name}

# Prepare udev rules file to allow normal user access to ethercat devices
mkdir -p ${RPM_BUILD_ROOT}/etc/udev/rules.d
echo 'KERNEL=="EtherCAT[0-9]*", MODE="0666"' > ${RPM_BUILD_ROOT}/etc/udev/rules.d/90-ethercat.rules


%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc COPYING.LESSER
%doc ChangeLog
%doc FEATURES
%doc INSTALL
%doc NEWS
%doc README
%doc README.EoE
/etc/init.d/ethercat
/etc/sysconfig/ethercat
/usr/bin/ethercat
/usr/lib64/libethercat.so*
/etc/udev/rules.d/90-ethercat.rules

%files devel
%defattr(-,root,root)
/usr/include/*.h
/usr/lib64/libethercat.a
/usr/lib64/libethercat.la

%files src
%defattr(-,root,root)
/usr/src/%{name}


#----------------------------------------------------------------------------
