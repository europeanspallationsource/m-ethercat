# File to download and unpack.
#TARBALL=ftp://xmlsoft.org/libxml2/libxml2-2.9.2.tar.gz
# Path where source tarball was unpacked.
SRCPATH=ethercat-1.5.2

PACKAGE_CONFIGURE_FLAGS="--with-pic --enable-generic --disable-8139too"

KERNEL_SRC="YES"
